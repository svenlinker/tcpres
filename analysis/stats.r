library(ggplot2)
library(mgcv)
## height and width of pdf output
gheight <- 10
gwidth<-10
dotsize <- 3
# directory names
pdfdir <- "pdf/"
graphdir <- "resync"
fulldir <- paste(pdfdir,graphdir, sep="")


sq_t5 = read.csv("square-t5.csv", 
              col.names=c("name","sensors", "zones","min","max", "diff", "max_zone_size", 
                          "distributions","readings",  "max_block", "t_whole", "t_z3","t_freqs", "t_partitions", 
                          "t_probtree","t_wfreqs"), header=FALSE)

sq_t10 = read.csv("square-t10.csv", 
              col.names=c("name","sensors", "zones","min","max", "diff", "max_zone_size", 
                          "distributions","readings",  "max_block", "t_whole", "t_z3","t_freqs", "t_partitions", 
                          "t_probtree","t_wfreqs"), header=FALSE)

square = rbind(sq_t5,sq_t10)

#rand = read.csv("random.csv", 
#              col.names=c("name","sensors", "zones","min","max", "diff", "max_zone_size", 
#                          "distributions","readings",  "max_block", "t_whole", "t_z3","t_freqs", "t_partitions", 
#                          "t_probtree","t_wfreqs"), header=FALSE)

ra_t5 = read.csv("random-t5.csv", 
                col.names=c("name","sensors", "zones","min","max", "diff", "max_zone_size", 
                            "distributions","readings",  "max_block", "t_whole", "t_z3","t_freqs", "t_partitions", 
                            "t_probtree","t_wfreqs"), header=FALSE)

ra_t10 = read.csv("random-t10.csv", 
                col.names=c("name","sensors", "zones","min","max", "diff", "max_zone_size", 
                            "distributions","readings",  "max_block", "t_whole", "t_z3","t_freqs", "t_partitions", 
                            "t_probtree","t_wfreqs"), header=FALSE)

rand = rbind(ra_t5,ra_t10)

dist_string = expression(paste("|",Theta, "|"))
max_readings_string = expression(rho)
num_readings_string = expression(paste("|",Theta["/"*R], "|"))
max_block_string = expression(paste("max ( |",Theta[bar(rho)], "| )"))
runtime_string = "Runtime (s)"
zones_string = "|Z|"

frequencies_m = t(matrix(c(
2,       0.012987012987,     
3,        0.0834879406308,     
4,       0.215213358071,    
5,       0.293135435993,  
6,       0.239332096475,    
7,       0.116883116883 ,    
8,       0.0333951762523,     
9,       0.00556586270872), nrow = 2))      
colnames(frequencies_m) = c("counts", "prob")

wfrequencies_m1 = t(matrix(c(
2,0.00059567190713,
3, 0.00866235591655,
4,0.0458385277123,
5,0.150838353486,
6,0.315106646102,
7,0.320763219302,
8,0.139326768518,
9,0.0188684570557), nrow=2))
colnames(wfrequencies_m1) = c("counts", "prob")

wfrequencies_m2 = t(matrix(c(
2,0.00375596309236,
3,0.0358396870506,
4,0.140588517589,
5,0.418704336146,
6,0.334857889802,
7,0.0647857275039,
8,0.00146089020927,
9,6.98860593647e-06), nrow=2))
colnames(wfrequencies_m2) = c("counts", "prob")

frequencies.df = as.data.frame(frequencies_m)
wfrequencies_m1.df = as.data.frame(wfrequencies_m1)
wfrequencies_m2.df = as.data.frame(wfrequencies_m2)

freq_m = ggplot(data = frequencies.df) + theme_bw() +  ylim(c(0,.5))+geom_col(aes(x=factor(counts), y=prob), fill = "red")  + labs(x = "Target Count", y = "Probability")
#print(freq_m)

wfreq_m1 = ggplot(data = wfrequencies_m1.df) + theme_bw() + ylim(c(0,.5))+ geom_col(aes(x=factor(counts), y=prob  ), fill = "red")  + labs(x = "Target Count", y = "Probability")
#print(wfreq_m1)

wfreq_m2 = ggplot(data = wfrequencies_m2.df)  + theme_bw() + ylim(c(0,.5))+ geom_col(aes(x=factor(counts), y=prob), fill = "red" )  + labs(x = "Target Count", y = "Probability")
#print(wfreq_m2)

ggsave("freqs.pdf", plot=freq_m, device="pdf", width=gwidth, height=gheight, units = "cm")
ggsave("wfreqs1.pdf", plot=wfreq_m1, device="pdf", width=gwidth, height=gheight, units = "cm")
ggsave("wfreqs2.pdf", plot=wfreq_m2, device="pdf", width=gwidth, height=gheight, units = "cm")


#### Plots for analysis of main data set
#### Random topologies and square topology


square_dist_time = ggplot (data =square[square$distributions < 100000,]) + 
  geom_smooth(method="lm", aes(x=distributions, y=t_z3), formula="y ~ poly(x,2)")+
  geom_point( aes(x=distributions, y=t_z3, color=readings))+
  scale_color_gradientn(colours = rainbow(7))+
  labs(x = dist_string, y = runtime_string, color = num_readings_string) +
  theme_bw()  +
  ggtitle(expression(paste("Square: |",Theta,"| vs. Runtime (s)")))
print(square_dist_time)
ggsave("square_dist_time.pdf", plot=square_dist_time, device="pdf", width=gwidth, height=gheight, units = "cm")


square_dist_sqr_time = ggplot (data =square[square$distributions < 100000,])  +
  geom_smooth(method="lm", aes(x=distributions, y=(t_z3)))+theme_bw()  + 
  geom_point( aes(x=distributions, y=(t_z3), color=readings))+ 
  scale_color_gradientn(colours = rainbow(7))+
  scale_y_sqrt()+
  labs(x = dist_string, y = expression(paste( sqrt("Runtime"), " (s)")), color = num_readings_string) +
  ggtitle(expression(paste("Square: |",Theta,"| vs. ",sqrt("Runtime"), " (s)")))
print(square_dist_sqr_time)
ggsave("square_dist_sqrt_time.pdf", plot=square_dist_sqr_time, device="pdf", width=gwidth, height=gheight, units = "cm")


rand_dist_time = ggplot (data =rand[rand$distributions < 100000,]) + 
  geom_smooth(method="lm", aes(x=distributions, y=t_z3), formula="y ~ poly(x,2)")+
  geom_point( aes(x=distributions, y=t_z3, color=readings))+
  scale_color_gradientn(colours = rainbow(7))+ 
  theme_bw()  +
  labs(x = dist_string, y = runtime_string, color = num_readings_string)+
  ggtitle(expression(paste("Random: |",Theta,"| vs. Runtime (s)")))
print(rand_dist_time)
ggsave("random_dist_time.pdf", plot=rand_dist_time, device="pdf", width=gwidth, height=gheight, units = "cm")

exp.model <-lm(t_z3 ~ exp(zones), rand)

rand_dist_sqr_time = ggplot (data =rand[rand$distributions < 100000 ,]) + 
  geom_smooth(method="lm", aes(x=distributions, y=t_z3), formula ="y ~ x")+
  scale_y_sqrt()+
  geom_point( aes(x=(distributions), y=(t_z3), color=readings))+
  scale_color_gradientn(colours = rainbow(7))+ 
  theme_bw()  +
  labs(x = dist_string, y = expression(paste( sqrt("Runtime"), " (s)")), color = num_readings_string)+
  ggtitle(expression(paste("Random: |",Theta,"| vs. ",sqrt("Runtime"), " (s)")))
print(rand_dist_sqr_time)
ggsave("random_dist_sqrt_time.pdf", plot=rand_dist_sqr_time, device="pdf", width=gwidth, height=gheight, units = "cm")



rand_z_time = ggplot (data =rand[rand$distributions <100000 & rand$t_z3 < 300,] ) + 
    geom_boxplot(aes(x=zones, y=t_z3, fill=factor(zones)))+
#  geom_smooth(method="lm", aes(x=zones, y=t_z3), formula ="y ~ exp(x)")+
  stat_smooth(method="nls",  method.args = list(formula=y ~ a*exp(b*x), start=list(a=.3, b=.0002)), aes( x=zones, y=t_z3),  color="red" ,se=FALSE) +
      #  scale_color_gradientn(colours = rainbow(7))+ 
  theme_bw()  +
  #  ylim(c(0,10))+
  guides(fill=FALSE)+
  labs(x = "|Z|", y = runtime_string, fill = "Zones")+
  ggtitle("Random: |Z| vs. Runtime (s)")
print(rand_z_time)
ggsave("random_z_t.pdf", plot=rand_z_time, device="pdf", width=gwidth, height=gheight, units = "cm")


## Boxplot: Random Topologies --- |S| vs |Z|
rand_s_z = ggplot (data =rand ) + 
  geom_boxplot(aes(x=(sensors), y=zones, fill=factor(sensors)) )+
  scale_color_gradientn(colours = rainbow(7))+ 
  theme_bw()  +
  scale_y_continuous(breaks=c(3,4,5,6,7,8,9,10,11,12,13,14), labels=c(3,4,5,6,7,8,9,10,11,12,13,14))+
  guides(fill=FALSE)+
  labs(x = "|S|", y = "|Z|")+
  ggtitle("Random: |S| vs. |Z|")
print(rand_s_z)
ggsave("random_s_z.pdf", plot=rand_s_z, device="pdf", width=gwidth, height=gheight, units = "cm")



## Boxplot: Random Topologies --- |Z| vs |dist|
exp.model <-lm(distributions ~ exp(zones), rand)
rand_z_dist = ggplot (data =rand[rand$distributions < 100000  & rand$t_z3 < 300,]) + 
  geom_boxplot(aes(x=(zones), y=distributions, fill=factor(zones)) )+
  stat_smooth(method="nls",  method.args = list(formula=y ~ a*exp(b*x), start=list(a=1000, b=.1)), aes( x=zones, y=distributions),  color="red" ,se=FALSE) +
  theme_bw()  +
  guides(fill=FALSE)+
  labs(x = "|Z|", y = dist_string, fill="")+
  ggtitle(expression(paste("Random: |Z| vs. |",Theta,"|")))
print(rand_z_dist)
ggsave("random_z_dist.pdf", plot=rand_z_dist, device="pdf", width=gwidth, height=gheight, units = "cm")

## Random Topologies --- readings vs max_block
rand_read_mblock = ggplot (data =rand ) + 
  geom_point(aes(x=readings, y=max_block, color=distributions) )+
  #stat_smooth(method="nls",  method.args = list(formula=y ~ a*exp(b*x), start=list(a=3000, b=.02)), aes( x=zones, y=distributions),  color="red" ,se=FALSE) +
  theme_bw()  +
  scale_color_gradientn(colours = rainbow(7))+ 
  guides(fill=FALSE)+
  labs(x = num_readings_string, y = max_block_string, color = dist_string, fill="")+
  ggtitle(as.expression(bquote("Random: "~"|"~Theta["/"*R]~"|"~" vs. "~"max ( |"~Theta[bar(rho)]~"| )")))
print(rand_read_mblock)
ggsave("random_read_mblock.pdf", plot=rand_read_mblock, device="pdf", width=gwidth, height=gheight, units = "cm")

## Square Topologies --- readings vs max_block
sq_read_mblock = ggplot (data =square ) + 
  geom_point(aes(x=readings, y=max_block, color=distributions) )+
  theme_bw()  +
  scale_color_gradientn(colours = rainbow(7))+ 
  guides(fill=FALSE)+
  labs(x = num_readings_string  , y = max_block_string, fill="", color=dist_string)+
ggtitle(as.expression(bquote("Square: "~"|"~Theta["/"*R]~"|"~" vs. "~"max ( |"~Theta[bar(rho)]~"| )")))
print(sq_read_mblock)
ggsave("square_read_mblock.pdf", plot=sq_read_mblock, device="pdf", width=gwidth, height=gheight, units = "cm")


