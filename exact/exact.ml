open Printf
open Z3.Symbol
open Z3.Expr
open Z3.Goal
open Z3.Boolean
open Z3.Arithmetic
open Z3.Arithmetic.Integer
open Z3.Solver
open Z3.Model
open Z3.FuncDecl
(* Convert topologies descriptions into sets of variables  *)

type sensor = String.t       

let rec copy_string s c =
  if (c == 0) then "" else s ^ (copy_string s (c-1))

let range a b = 
  let rec aux a b =
    if a > b then [] else a :: aux (a+1) b in
  aux a b 
                                 
                
module Zone = struct
  include Set.Make(String)

  let to_string z =
  if (is_empty z) then
    "{}"
  else
    String.concat "" (elements z)                             

 let remove_elt (z,r) l =
  let rec go l acc = match l with
    | [] -> List.rev acc
    | (z1,r1)::xs when  (equal z1 z) -> go xs acc
    | x::xs -> go xs (x::acc)
  in go l []

 let rec find_max_reading ez l = match l with
   | [] -> ez
   | (z,r) :: tl when (equal (fst ez) z) && (r > (snd ez)) -> find_max_reading (z,r) tl
   | hd :: tl -> find_max_reading ez tl

 let remove_duplicates l =
   let rec go l acc = match l with
     | [] -> List.rev acc
     | x :: xs -> go (remove_elt x xs) ((find_max_reading x l) ::acc)
   in go l []                        
                                  
end
                
module Distr = struct
  include Map.Make(Zone)   

  let to_string (distr ) =
    if (is_empty distr) then
      "empty"
    else
      fold (fun z r -> (^) ("("^ (Zone.to_string z) ^ "\t--> "^(string_of_int  r)^")\n")   )    distr ""

  let to_string_indent distr n =
    if (is_empty distr) then
      (copy_string "\t" n) ^ "empty"
    else
      fold (fun z r -> (^) ((copy_string "\t" n) ^ "("^ (Zone.to_string z) ^ "\t--> "^(string_of_int  r)^")\n")   )    distr ""

  let is_subdistr d s =
    for_all (fun z1 t1 -> exists (fun z2 t2 -> z1==z2 && t1 <= t2) s) d

  (* a reduction contains exactly the target counts of its extension, but lacks the empty zones *)
  let is_reduction d s =
    for_all (fun z1 t1 -> exists (fun z2 t2 -> (z1==z2 && t1 == t2) || t2 == 0 ) s) d
              
  let get_supers d distrs =
    let sub_distr = 
      List.filter (is_subdistr d) distrs
    in
    sub_distr

  let get_extension d distrs =
    let extensions = List.filter (is_subdistr d) distrs in
    match extensions with
    | [] -> failwith ("No suitable extension found for " ^ (to_string d))
    |[sing] -> sing
    | hd :: sec :: tl -> failwith ("More than one suitable extension found for "^ (to_string d))

  let get_non_empty_zones z d =
    List.filter (fun (zone,t) -> t > 0 && ((not (Zone.equal zone (fst z))) || (t <= snd z))) (bindings d)
                                  
  let count_targets distr =
    fold (fun k r -> (+) r ) distr 0                                  
end
                 
(* Weights as maps from zones to float *)
module W = struct
  include Map.Make (Zone)

  let to_string w =
    if (is_empty w) then
      "none"
    else
      fold (fun zone weight -> (^) ((Zone.to_string zone) ^":\t"^(string_of_float weight)^"\t")) w ""           
end

(* a tree where each node may have arbitrary many children  *)  
type 'a tree =
  | Leaf of 'a                               
  | Node of 'a * 'a tree list

(* 
probability tree for creating distributions.
(p,z,d):
p: probability according to the weights of the zones that could extend the distribution of the parent
z: zone that was chosen to extend the distribution of the parent
d: extension of the parent distribution with a new target in zone z

We have to enforce that the sum of the probabilites of the 
children of a node sum up to 1 (if there are any)
 *)      

module Prob = struct
  type t = (float * Zone.t * int Distr.t) tree

  let rec to_string  (tree) depth =
    let tabs = (copy_string "\t" depth) in
    match tree with
    | Leaf(w,z,d) ->  tabs ^ "(Zone: "^ (Zone.to_string z) ^"\tProb: " ^ (string_of_float w)^"\tDistr:\n "^(Distr.to_string_indent d (depth+4)) ^tabs^")\n"
    | Node((w,z,d),l) ->
       let children = List.fold_right (fun t -> (^) (to_string t (depth+1))) l "" in 
       tabs ^ "(Zone: "^ (Zone.to_string z) ^ "\tProb:" ^ (string_of_float w) ^ "\tDistr:\n"^(Distr.to_string_indent d (depth+4)) ^ "\n"^tabs^"Children: [\n"^ children ^ tabs^"])\n"
                                                                                                                                                                                 
                                                                                                                                                                                 
  let rec to_string_indent  (tree) depth =
    let tabs = (copy_string "\t" depth) in
    match tree with
    | Leaf(w,z,d) ->  tabs ^ "(Zone: "^ (Zone.to_string z) ^"\tProb: " ^ (string_of_float w)^"\tDistr:\n "^(Distr.to_string_indent d (depth+4)) ^tabs^")\n"
    | Node((w,z,d),l) ->
       let children = List.fold_right (fun t -> (^) (to_string_indent t (depth+1))) l "" in 
       tabs ^ "(Zone: "^ (Zone.to_string z) ^ "\tProb:" ^ (string_of_float w) ^ "\tDistr:\n"^(Distr.to_string_indent d (depth+4)) ^ "\n"^tabs^"Children: [\n"^ children ^ tabs^"])\n"

  let compute weights distributions =
    let rec mk_children distrs p d z  =
      let pdistr = Distr.get_supers d distrs in
      let unique_zones = List.map (Distr.get_non_empty_zones z)  pdistr
                         |> List.concat |> Zone.remove_duplicates
      in
      let extension_zones = unique_zones
                            |> List.filter (fun (zone,reading) -> not ( Distr.exists (fun dzone dread -> ((Zone.equal dzone zone) && dread ==reading)) d  ) ) 
      in
      let wsum = List.fold_right (fun (z,r) -> (+.) (W.find z weights)) extension_zones 0. in
      match extension_zones with
      | [] -> Leaf(p, fst z, Distr.get_extension d pdistr )
      | hd :: tl ->
         let add_zone = fun reading ->
           match Distr.find_opt (fst reading) d with
           | None -> 
              Distr.add (fst reading) (1) d
           | Some(r) -> 
              Distr.add (fst reading) (r+1) d
         in
       if  List.length pdistr = 1 then
         Leaf(p,fst z,Distr.get_extension d pdistr)
       else
         let subtrees = List.map (fun zone -> mk_children  pdistr ((W.find (fst zone) weights) /. wsum) (add_zone zone) zone) extension_zones in
         Node((p, fst z, d), subtrees)
    in
    mk_children  distributions  1. Distr.empty  (Zone.empty, 0)

  let get probs d = 
    let rec aux probs dist p = match probs with
      | Leaf(pd, _, distr) -> if (Distr.equal (=) (distr) (dist)) then
                                pd*.p
                              else
                                0.
      | Node((pd, z, distr), children) -> if (Distr.is_subdistr distr dist) then 
                                            p *. (List.fold_right (fun c -> (+.) (aux c dist (pd))) children 0.)
                                          else  0.
    in
    aux probs d 1.                                                                                                                                                                           
end
                                                      
let rec read_lines out file =
  try read_lines ((input_line file) :: out) file with
  | End_of_file -> (close_in file; List.rev out)

let parse file =
  open_in file
  |> read_lines []

let tokenize = String.split_on_char ' '

let tokenize_zone line =
  let tokens = (String.split_on_char ' ' line) in
  match tokens  with
  |[] | [_]| [_;_] -> failwith ("Error: Invalid input file! Reason: invalid zone description " ^ line)
  |weight :: target :: desc -> (float_of_string weight, int_of_string target, desc) 

let parse_topology fname =
  match parse fname with
  | [] | [_] -> failwith ("Error: Invalid input file " ^ fname)
  | x :: y :: xs ->
     let ids = 
       tokenize x
     in
     let readings =
       tokenize y
       |> List.map2 (fun s r -> (s, int_of_string r)) ids 
     in
     let rel =
       List.map tokenize_zone xs
     in
     (ids, readings, rel, List.length xs)

let rem_meta (w,t,desc) =
   Zone.of_list desc
  
let mk_zones raw_zones =
  let zones = List.map rem_meta raw_zones in
  let rec aux raw_zones weights distr = match raw_zones with
    | [] -> (weights, distr)
    | (w,t,desc) :: tl ->
       let z = Zone.of_list desc in
       aux tl (W.add z w weights) (Distr.add z t distr)  
  in
  let (w,d) = aux raw_zones W.empty Distr.empty in
  (w, d, zones)
       
       
let mk_zone_consts ctx =
  List.map (fun z -> (z, mk_string ctx (String.concat "" (Zone.elements z)))) 

let rec mk_sensor_sum ctx domain zones sensor =
  match zones with
  | [] -> (mk_numeral_int ctx 0 domain)
  | hd :: tl ->
     let (name, symb) = hd in
     if (Zone.exists ((String.equal) sensor) name)
     then mk_add ctx [(Z3.Expr.mk_const ctx symb domain) ; (mk_sensor_sum ctx domain tl sensor)]
     else  mk_sensor_sum ctx domain tl sensor
                         
let mk_sensor_equation ctx domain zones reading =
  let (sensor, value) = reading in
  mk_eq ctx (mk_numeral_int ctx value domain) (mk_sensor_sum ctx domain zones sensor) 

        
let mk_topologic_constraints ctx domain readings zones  = 
  let pos =
    List.map (fun (_,z) -> mk_ge ctx (Z3.Expr.mk_const ctx z domain) (mk_numeral_int ctx 0 domain)) zones
  in
  let eqs =
    List.map (fun r -> mk_sensor_equation ctx domain zones r) readings
  in
  List.append pos eqs

let model_to_constraints ctx domain model =
  match model with
  | None -> mk_true ctx
  | Some m -> 
     let funcs = get_const_decls m in
     let interpret = fun f ->
       match (get_const_interp m f) with
       | None -> Z3.Expr.mk_numeral_int ctx 0 domain 
       | Some exp -> exp
     in
     let negated = fun f -> mk_not ctx (mk_eq ctx (Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name f)  domain)  (interpret f))
     in     
     mk_or ctx (List.map negated funcs)

                    
let rec solve_constraints ctx domain solver solutions =
  let status = check solver [] in
  match status with
  | UNSATISFIABLE -> solutions
  | UNKNOWN -> solutions
  | SATISFIABLE ->
     let m = get_model solver
     in
     let negated = model_to_constraints ctx domain m in
     add solver [negated];
     solve_constraints ctx domain solver (m :: solutions) 
                       
let mk_solutions ctx domain solver eqs =
  add solver eqs;
  solve_constraints ctx domain solver  []
    

let find_zone_name zones symbol =
  let name = Z3.Symbol.get_string symbol in
  let rec aux zones = match zones with
    | [] -> None
    | (n,s) :: tl -> if (String.equal (Z3.Symbol.get_string s) name) then Some n else aux tl 
  in
  match aux zones with
  | None -> failwith ("something went seriously wrong while finding zone "^name)
  | Some (n) -> n
      
let model_to_distribution zones model =
    match model with
  | None -> Distr.empty
  | Some m ->
     let funcs = get_const_decls m in
     let interp_to_int = fun f ->
       match get_const_interp m f with
       | None -> 0
       | Some expr -> Integer.get_int expr
     in
     let add_to_distr f distr =
       Distr.add (find_zone_name zones (Z3.FuncDecl.get_name f)) (interp_to_int f) distr
     in
     let rec aux funcs distr = match funcs with
       | [] -> distr
       | hd :: tl -> aux tl (add_to_distr hd distr)  
     in
     aux funcs Distr.empty

                                                                                               
let mk_frequencies distributions =
  let counts = List.map Distr.count_targets distributions in
  let num_solutions =  List.length distributions in
  let max = List.fold_left (max) 0 counts in
  let frequencies = range 0 max in
  List.map (fun x ->
      let count = List.filter ((=) x) counts
                  |> List.length
      in
      (x, count , float_of_int count /. float_of_int num_solutions ) ) frequencies
  |> List.filter (fun (rank, sol, p) -> sol > 0) 


let mk_weighted_frequencies probtree distributions =
  let counts = List.map Distr.count_targets distributions in
  let max = List.fold_left (max) 0 counts in
  let frequencies = range 0 max in
  let probabilities=  List.map (fun d ->
                          let prob = Prob.get probtree d in
                          (d,  prob) ) distributions
  in
  List.map (fun x ->
      let count = List.filter ((=) x) counts
                  |> List.length in
      (x, count, 
       List.fold_right (fun (d,p) -> (+.) (if (Distr.count_targets d) == x then p else 0.)) probabilities 0.)  
    )                         frequencies
  |> List.filter (fun (rank, sol, p) -> sol > 0) 


let freq_to_string freq =
  List.fold_right (fun (n,c, p) -> (^) ((string_of_int n) ^ ":\t "^(string_of_int c)^"\t"^(string_of_float p)^"\n"))  freq "" 
                 

let directory = ref ""
let output = ref "result.txt"

let usage_msg = "compute all possible exact sensor readings for given topology"
                 
let specs = [
    ("-d", Arg.String (fun s -> directory := s), "analyse all topologies in given directory");
    ("-o", Arg.String (fun s -> output := s), "output file")
  ]

              
  let analyse_file oc top_file =
    let (sensors, readings, raw_zones, top_string) = parse_topology top_file in
    let (weights, targets, zones) = mk_zones raw_zones in
    let cfg = [("model", "true"); ("proof", "false")] in
    let ctx = Z3.mk_context cfg in
    let domain = Integer.mk_sort ctx in
    let zone_vars = mk_zone_consts ctx zones in
    let goal = mk_topologic_constraints ctx domain readings zone_vars in
    let solver = mk_simple_solver ctx in
    let t_before_solving = Sys.time() in
    let solutions = mk_solutions ctx domain solver goal in
    let t_after_solving = Sys.time() in 
    let distributions = List.map (model_to_distribution zone_vars) solutions in
    let contains_original = List.exists (fun d -> Distr.equal (=) targets d) distributions in
    fprintf oc "file: %s\n" top_file;
    flush oc;
    List.iter (fun d -> (fprintf oc "%s" (Distr.to_string d));fprintf oc "\n" ) distributions;
    let t_before_freqs = Sys.time() in
    let frequencies = mk_frequencies distributions in
    let t_after_freqs = Sys.time() in
    fprintf oc "unweighted frequencies:\n";
    fprintf oc "%s\n" (freq_to_string frequencies);
    flush oc;
    let t_before_probs = Sys.time() in
    let probs = Prob.compute weights distributions in
    let t_after_probs = Sys.time() in
    let wfreq = mk_weighted_frequencies probs distributions in
    let t_after_wfreqs = Sys.time() in
    let t_solving = t_after_solving -. t_before_solving in
    let t_freqs = t_after_freqs -. t_before_freqs in
    let t_probs = t_after_probs -. t_before_probs in
    let t_wfreqs = t_after_wfreqs -. t_after_probs in
    let t_whole = t_after_wfreqs -. t_before_solving in

    fprintf oc "weighted frequencies:\n";
    fprintf oc "%s\n" (freq_to_string wfreq);
    fprintf oc "ground truth contained in list of solutions: %b\n" contains_original ;
    if contains_original then
      let (n,f,p) =  (List.find (fun (n,f,p) -> ((=) n (Distr.count_targets targets))) wfreq) in
      fprintf oc "probability of ground truth: %f\n" p
    else ();
    fprintf oc "\nTime to compute: %f\n" t_whole;
    fprintf oc "solutions: %f\t frequencies: %f\t probs:%f\t weighted freqs:%f\n" t_solving t_freqs  t_probs t_wfreqs;
    fprintf oc "=====================================================\n\n";
    flush oc;
    (distributions, frequencies, wfreq)
              

  let mk_analysis oc res =
    let is_unique row = match row with
      | (_,1,_) -> true
      | _ -> false
    in
  let unique_target_counts = List.filter (fun (dists, f,wf) -> List.length f = 1) res in
  let unique_distrs = List.filter (fun (dists, f,wf) -> (List.hd f)                                  
                                                 |> is_unique )
                        unique_target_counts
  in
  fprintf oc "unique number of targets: %d\n" (List.length unique_target_counts);
  fprintf oc "unique solutions: %d\n" (List.length unique_distrs);
  ()

let mk_full_path dir f =
  if Char.equal dir.[String.length dir -1] '/' then 
    dir^f
  else
    dir^"/"^f

  
let () =
  Arg.parse specs print_endline usage_msg;
  let files = Sys.readdir !directory
              |> Array.to_list 
              |> List.sort String.compare in
  let oc = open_out !output in
  (*  let results = List.map (fun f ->  analyse_file oc (mk_full_path !directory f)) files*)
  let results = List.map (fun f -> mk_full_path !directory f
                                   |>  analyse_file oc ) files
  in
  mk_analysis oc results;
  close_out oc
