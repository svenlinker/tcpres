open List

type t

(** checks whether this zone is covered by the given sensor. The sensor is
given by its string representation *)       
val contains : t -> string -> bool

                                                                
