open Printf
   
module type Debug =
  sig
    val debug : string -> unit
  end
   
let rec copy_string s c =
  if (c == 0) then "" else s ^ (copy_string s (c-1))

let range a b = 
  let rec aux a b =
    if a > b then [] else a :: aux (a+1) b in
  aux a b 

(* there has to be a better way of doing this! *)
let count_occurrence l el = 
  let rec aux  occ l el = match l with
    | [] -> occ
    | hd :: tl -> if (el == hd) then aux (occ+1) tl el else aux occ tl el
  in
  aux 0 l el

let find_max extract l =
  match l with
| [] -> failwith "None"
| h :: t ->
   let rec helper (seen, rest) =
     match rest with
     | [] -> seen
     | h' :: t' ->
        let value = extract h' in
        let seen' = if value > seen then value else seen in
        let rest' = t' in
        helper (seen', rest')
   in helper (extract h, t)

  
let log oc s =
  fprintf oc "%s" s

let rec log_csv oc l =
  match l with
  | [] -> ()
  | [el] -> fprintf oc "%s\n" el
  | hd :: tl -> fprintf oc "%s," hd; log_csv oc tl

let  hsh_to_list h =
  Hashtbl.fold (fun k v acc -> (k, v) :: acc) h []
                
 
module SolveUtil = 
  struct
    type c = Z3.context
    type s = Z3.Sort.sort
    type m = Z3.Model.model
    type e = Z3.Expr.expr
           
    let model_to_constraints ctx domain model =      
      let open Z3.Boolean in
      let open Z3.Expr in
      let open Z3.Model in
      match model with
      | None -> mk_true ctx
      | Some m -> 
         let funcs = get_const_decls m in
         let interpret = fun f ->
           match (get_const_interp m f) with
           | None -> Z3.Expr.mk_numeral_int ctx 0 domain 
           | Some exp -> exp
         in
         let negated = fun f -> mk_not ctx (mk_eq ctx (Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name f)  domain)  (interpret f))
         in     
         (mk_or ctx (List.map negated funcs)) 
  end           

  
  
module ReadingProb = struct
  include Map.Make(struct type t = int
                               let compare = Pervasives.compare end)      
  let to_string rprob =
    fold (fun r p -> (^) ((string_of_int r)^": "^(string_of_float p)^"\t")) rprob "" 

  let is_consistent rprob =
    abs_float (fold (fun r p -> (+.) p) rprob 0. -. 1.) < 0.000001 

end
                  
module SensorsProb = struct
  include Map.Make(String)

  let to_string sprob =
    fold (fun s rprop -> (^) (s ^ "--> "^(ReadingProb.to_string rprop)^"\n")) sprob ""

  let is_consistent sprob =
    fold (fun s rp -> (&&) (ReadingProb.is_consistent rp)) sprob true 
end
  
  
module Distr =
  struct
    include Map.Make(Zone)
    let print distr =
      if (is_empty distr) then
        "empty"
      else
        fold (fun z r -> (^) ("("^ (Zone.print_zone z) ^ "\t--> "^(string_of_int  r)^")\n")) distr ""

    let print_indent (distr) n =
      if (is_empty distr) then
        (copy_string "\t" n) ^ "empty"
      else
        fold (fun z r -> (^) ((copy_string "\t" n) ^ "("^ (Zone.print_zone z) ^ "\t--> "^(string_of_int  r)^")\n")   )    distr ""

    let is_reduction d s =
      for_all (fun z1 t1 -> exists (fun z2 t2 -> (z1==z2 && t1 == t2)  ) s) d
    let is_subdistr d s =
      for_all (fun z1 t1 -> exists (fun z2 t2 -> z1==z2 && t1 <= t2) s) d

    let get_extension d distrs =
      let extensions = List.filter (is_subdistr d) distrs in
      match extensions with
      | [] -> failwith ("No suitable extension found for " ^ (print d) ^ "in \n"^ (List.fold_right (fun e -> (^) ((print e) ^ "\n"))) distrs "")
      |[sing] -> sing
      | hd :: sec :: tl -> failwith ("More than one suitable extension found for "^ (print d))
              

    let is_subdistr_fixed fixed d s =
      for_all (fun z1 t1 -> if (List.exists (fun (z2 ,t2) -> (Zone.equal z1 z2)) fixed)
                            then
                              exists (fun z2 t2 -> z1==z2 && t1 == t2) s 
                            else
                              exists (fun z2 t2 -> z1==z2 && t1 <= t2) s
        ) d

    let  get_possible_supers_fixed fixed d distrs =
      let sub_distr = 
        List.filter (is_subdistr_fixed fixed d) distrs
      in
      sub_distr

    let  get_possible_supers d distrs =
      let sub_distr = 
        List.filter (is_subdistr d) distrs
      in
      sub_distr
        
    let count_targets distr =
      fold (fun k r -> (+) r ) distr 0
    let perceived_targets d s =
      fold (fun z r -> (+) (if Zone.exists ((String.equal) s)  z then r else 0)) d 0  

(*let is_equal_mod_zone d s pres =
  Distr.for_all (fun z1 t1 ->  (Zone.equal z1 (fst pres) ) ||  Distr.exists (fun z2 t2 -> (z1==z2 && t1 == t2)) s) d
  &&
  Distr.for_all (fun z1 t1 ->  (Zone.equal z1 (fst pres)) || Distr.exists (fun z2 t2 -> (z1==z2 && t1 == t2)) d) s
 *)
  end


module Counts =
  struct
    include Map.Make(String)
    let print c =
      fold (fun s t -> (^) (s ^ ": "^ (string_of_int t)^"\t")) c ""
  end


(* Weights as maps from zones to float *)

module W =
  struct
    include Map.Make (Zone)
                     
    let print w =
      if (is_empty w) then
        "none"
      else
        fold (fun zone weight -> (^) ((Zone.print_zone zone) ^":\t"^(string_of_float weight)^"\t")) w ""
  end

    
(* a tree where each node may have arbitrary many children  *)  
type 'a tree =
  | Leaf of 'a                               
  | Node of 'a * 'a tree list

(* 
probability tree for creating distributions.
(p,z,d):
p: probability according to the weights of the zones that could extend the distribution of the parent
z: zone that was chosen to extend the distribution of the parent
d: extension of the parent distribution with a new target in zone z

We have to enforce that the sum of the probabilites of the 
children of a node sum up to 1 (if there are any)
 *)      
                 


