open Util
   
module Make :
functor (D : Debug) ->
sig
  val read_lines : string list -> in_channel -> string list
  val parse : string -> string list
  val tokenize : string -> string list
  val tokenize_zone : string -> float * int * string list
  val mk_rprob :
    'a Util.ReadingProb.t ->
    Util.ReadingProb.key list -> 'a list -> 'a Util.ReadingProb.t
  val parse_rdist :
    float Util.ReadingProb.t Util.SensorsProb.t ->
    String.t list ->
    (String.t * int) list ->
    (String.t * int) list ->
    string list ->
    float Util.ReadingProb.t Util.SensorsProb.t *
      (float * int * string list) list * int
  val parse_topology :
    string ->
    string list * (string * int) list * (string * int) list *
      (float * int * string list) list * int *
        float Util.ReadingProb.t Util.SensorsProb.t
  val mk_zones :
    ('a * 'b * Zone.elt list) list -> 'a Util.W.t * 'c list * Zone.t list

  val mk_zone_consts : Z3.context -> Zone.t list -> (Zone.t * Z3.Symbol.symbol) list

  val mk_topologic_constraints : Z3.context ->
                                 Z3.Sort.sort ->
                                 (String.t * int) list ->
                                 (String.t * int) list ->
                                 int -> (Zone.t * Z3.Symbol.symbol) list -> Z3.Expr.expr list
end
