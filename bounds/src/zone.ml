(*module Zone : sig
  type t
  val is_empty : t -> bool
  val elements : t -> string list
  val print_zone : t -> String.t
end  = *)
(*  struct*)
  include Set.Make(String)

  let empty = empty
  let print_zone z = 
    if (is_empty z) then
      "{}"        
    else
      String.concat "" (elements z)                             

  let rem_meta (w,t,desc) =
   of_list desc
 
                    (*end*)

  let print_target_presence p =
  "("^(print_zone (fst p))^"\t--> "^string_of_int (snd p)^")"
