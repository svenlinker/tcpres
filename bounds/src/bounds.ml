open List
open Printf
open Util
open Uniwit
open Parser
   
let directory = ref ""
let output = ref "result"
let single = ref ""
let ignore_weights = ref false
let sruns =  ref 0
let verbose = ref false
let uniform = ref false
let witnesses = ref 100 
let log = (open_out "log/log.txt", open_out "log/log.csv") 

let debug s = match !verbose with
    false -> ()  | true -> (print_endline s)

module Uni = Uniwit.Make(struct  let debug = debug end)              
module Parse = Parser.Make(struct let debug = debug end)          
module Tree = Probtree.Make(struct let debug = debug end)
             
let rec solve_constraints i ctx domain  solver solutions =
  if (i mod 5000 =0) then
    (debug("Z3 found at least "^(string_of_int i)^" solutions."))  else (); 
  let status = Z3.Solver.check solver [] in
  match status with
  | UNSATISFIABLE -> solutions
  | UNKNOWN -> solutions
  | SATISFIABLE ->
     let m = Z3.Solver.get_model solver in
     let negated = SolveUtil.model_to_constraints ctx domain m  in
     Z3.Solver.add solver [negated];
     solve_constraints (i+1) ctx domain  solver (m :: solutions) 

(* with roughly more than 170000 we get a stack overflow *)                     
let solve_constraints_iter ctx domain solver =
  let solutions = ref [] in
  let i = ref 0 in
  while (Z3.Solver.check solver [] = SATISFIABLE) && !i < 100000  do
    let m = Z3.Solver.get_model solver in
    let negated = SolveUtil.model_to_constraints ctx domain m in
    solutions := m :: !solutions;  
    i := !i + 1;
    Z3.Solver.add solver [negated];
    if (!i mod 5000 =0) then
      (print_string "Z3 found at least ";print_int !i;print_endline " solutions.")  else (); 
  done;
  if (!i >= 100000) then print_endline "not all solutions found!!!" else ();
  !solutions

  
let mk_solutions ctx domain solver eqs =
  Z3.Solver.add solver eqs;
  solve_constraints_iter ctx domain solver


let mk_solutions_uniform ctx domain solver n eqs iter  =
  let solutions = ref [] in
  let i = ref 0 in
  while (!i < iter) do
    let result = Uni.uniwit ctx domain solver eqs n 1. in
    solutions := result::!solutions;
    i := !i +1
  done;
  debug ("generated "^(string_of_int (List.length !solutions))^" solutions");
  !solutions
 
  
let mk_frequencies distributions =
  let counts = List.map Distr.count_targets distributions in
  let num_solutions =  List.length distributions in
  let max = List.fold_left (max) 0 counts in
  let frequencies = range 0 max in
  List.map (fun x ->
    let count = Util.count_occurrence counts x in
    (x, count , float_of_int count /. float_of_int num_solutions ) ) frequencies
  |> List.filter (fun (rank, sol, p) -> sol > 0) 

let find_zone_name zones symbol =
  let name = Z3.Symbol.get_string symbol in
  let rec find_zone_aux zones = match zones with
    | [] -> None
    | (n,s) :: tl -> if (String.equal (Z3.Symbol.get_string s) name) then Some n else find_zone_aux tl 
  in
  match find_zone_aux zones with
  | None -> failwith ("something went seriously wrong while finding zone "^name)
  | Some (n) -> n
      
let model_to_distribution zones model =
    match model with
  | None -> Distr.empty
  | Some m ->
     let funcs = Z3.Model.get_const_decls m in
     let interp_to_int = fun f ->
       match Z3.Model.get_const_interp m f with
       | None -> 0
       | Some expr ->  Z3.Arithmetic.Integer.get_big_int expr |> Big_int.int_of_big_int
     in
     let add_to_distr f distr =
       Distr.add (find_zone_name zones (Z3.FuncDecl.get_name f)) (interp_to_int f) distr
     in
     let rec model_aux funcs distr = match funcs with
       | [] -> distr
       | hd :: tl -> model_aux tl (add_to_distr hd distr)  
     in
     model_aux funcs Distr.empty

  
let mk_sensor_counts sl d =
  List.fold_right  (fun s c  -> Counts.add s (Distr.perceived_targets d s) c) sl Counts.empty       

let mk_prob sprobs c =
  Counts.fold (fun s t -> ( *.) (ReadingProb.find t (SensorsProb.find s sprobs))) c 1.

let mk_partition slist sprobs dlist =
  let rawcounts = List.sort_uniq (Counts.compare (Pervasives.compare)) (List.map (fun d -> mk_sensor_counts slist d) dlist) in
  let counts = List.map (fun c -> (c, mk_prob sprobs c, List.filter (fun d -> (Counts.equal (=) c (mk_sensor_counts slist d))) dlist)) rawcounts in
  let sum = List.fold_right (fun (c,p,ds) -> (+.) p) counts 0. in
  List.map (fun (c,p,ds) -> (c,p/.sum, ds)) counts

let mk_partition2 slist sprobs dlist =
  let part = Hashtbl.create (List.length dlist) in
  List.iter (fun d ->
      let c = mk_sensor_counts slist d in
      match Hashtbl.find_opt part c with
        None -> Hashtbl.add part c (mk_prob sprobs c, [d])
      | Some(res) -> Hashtbl.replace part c (fst res, (d :: snd res))
    )
    dlist;
  let sum = Hashtbl.fold (fun c r -> (+.) (fst r))  part 0. in
  Hashtbl.fold (fun k v acc -> (k, fst v /. sum, snd v) :: acc) part []

let get_prob sensors wdl d =
  let counts = mk_sensor_counts sensors d in
  let (count, p, ds)  = List.find (fun (c,p,pt) -> Counts.equal (=) c counts) wdl in
  let result = List.find_opt (fun (dp, pp) -> Distr.equal (=) d dp) ds
  in
  match result with
  | None -> (*print_endline ("Could not find\n"^(Distr.print d));*)
            0. 
  | Some (dr, pt) -> pt
                  
let mk_weighted_frequencies sensors ls distributions =
  let counts = List.map Distr.count_targets distributions in
  let max = List.fold_left (max) 0 counts in
  let frequencies = range 0 max in
  let probabilities=  List.map (fun d ->
                          let prob = get_prob sensors ls d in
                          (d,  prob) ) distributions
  in
  List.map (fun x ->
      let count = count_occurrence counts x in
      (x, count, 
      List.fold_right (fun (d,p) -> (+.) (if (Distr.count_targets d) == x then p else 0.)) probabilities 0.)  
    )                         frequencies
  |> List.filter (fun (rank, sol, p) -> sol > 0)

  
  
let freq_to_string freq =
  List.fold_right (fun (n,c, p) -> (^) ((string_of_int n) ^ ":\t "^(string_of_int c)^"\t"^(string_of_float p)^"\n"))  freq "" 


let find_max_block l = 
  match l with
   [] -> failwith "No blocks in partition!"
  |(c,p,list)::t ->  let rec helper (seen,rest) =
              match rest with 
              [] -> seen
              |(c,p,list)::t' ->
                let h' = List.length list in
                let seen' = if  h' > seen then h' else seen in 
                         let rest' = t'
              in helper (seen',rest')
            in helper (List.length list,t)     

                      
let extract_block b = match b with 
  | (_,_,l) -> List.length l

let extract_reading r = match r with
  | (_,v) -> v

let extract_zone_size z = Zone.elements z |> List.length
           
let find_max_diff a b =
  let comb = List.combine a b in
  let extract_diff mm = match mm with
      ((_,v1),(_,v2)) -> v2 - v1
  in
  find_max extract_diff comb


(* CSV format: 
name, #sensors, #zones, max min, max max, diff, max_zone_size, #solutions, #diff sensor readings, size of largest block, t_whole, t_z3, t_freqs, t_partitions, t_probtree, t_wfreqs

 *)

let print_csv oc name sensors zones min max max_zone_size partition t_whole t_z3 t_freqs t_partitions t_probtree t_wfreqs =
  let solutions = List.fold_right (fun (c,p,block) -> (+) (List.length block)) partition 0 in
  fprintf oc "%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f\n" name (List.length sensors) (List.length zones) min max (max - min) max_zone_size solutions (List.length partition) (find_max extract_block partition) t_whole t_z3 t_freqs t_partitions t_probtree t_wfreqs;
  flush oc
           
                           
let analyse_file oc csvc top_file =
  let (sensors, min, max, raw_zones, top_string, sprobs) = Parse.parse_topology top_file in
  let (min_max, max_max) = (find_max extract_reading min, find_max extract_reading max) in
  let max_diff = find_max_diff min max in
  if (not (SensorsProb.is_consistent sprobs)) then failwith ("reading distribution for file "^top_file^" is inconsistent!") else ();
  let (weights, targets, zones) = Parse.mk_zones raw_zones in
  let max_zone_size = find_max extract_zone_size zones in
  fprintf oc "file: %s\n" top_file;
  fprintf oc "number of sensors:%d\n" (List.length sensors);
  fprintf oc "number of zones:%d\n" (List.length zones);
  fprintf oc "maximal min:%d \tmaximal max:%d\tmaximal diff:%d\n" min_max max_max max_diff;
  fprintf oc "maximal zone size: %d\n" max_zone_size;
  flush oc;
  let cfg = [("model", "true"); ("proof", "false")] in
  let ctx = Z3.mk_context cfg in
  let domain = Z3.Arithmetic.Integer.mk_sort ctx in
  let zone_vars = Parse.mk_zone_consts ctx zones in
  let goal = Parse.mk_topologic_constraints ctx domain min max max_max zone_vars in
  let solver = Z3.Solver.mk_solver ctx (Some(Z3.Symbol.mk_string ctx "QF_LIA"))  in
  (*  Z3.set_global_param "parallel.enable" "true";*)
  (if (!uniform) then fprintf oc  "creating %d uniformly distributed witness distributions\n" !witnesses
   else
     fprintf oc "creating all distributions\n"
  );
  let t_before_solving = Sys.time() in
  let solutions =  if (!uniform) then
                     mk_solutions_uniform ctx domain solver (int_of_float (2. ** (float_of_int (List.length zones)))) goal !witnesses 
                   else
                     mk_solutions ctx domain solver goal
  in
  let t_after_solving = Sys.time() in
  debug("raw solutions:"^(string_of_int (List.length solutions)));
  let distributions_raw = List.filter (fun d -> not (Distr.is_empty d)  ) ( List.map (model_to_distribution zone_vars) solutions) in
  let distributions = List.sort_uniq (Distr.compare (Pervasives.compare))  distributions_raw in 
  fprintf oc "number of solutions: %d\n" (List.length distributions)  ;
  flush oc;
  let t_before_freqs = Sys.time() in
  let frequencies = mk_frequencies distributions in
  let t_after_freqs = Sys.time() in
  fprintf oc "unweighted frequencies:\n";
  fprintf oc "%s\n" (freq_to_string frequencies);
  flush oc;
  let t_before_partition = Sys.time() in
  let part = mk_partition2 sensors sprobs distributions in
  let t_after_partition = Sys.time() in
  fprintf oc "different sensor readings: %d\n" (List.length part);
  fprintf oc "largest block: %d\n\n" (find_max extract_block part);
  flush oc;
  let t_before_probs = Sys.time() in
  let probs = if(!ignore_weights) then [] else if (!sruns <= 0) then 
                Tree.mk_probability_list part weights
              else
                Tree.mk_simulated_probability_list log part weights !sruns 
  in
  let t_after_probs = Sys.time() in
    
  let wfreq = if (!ignore_weights) then [] else  mk_weighted_frequencies sensors probs distributions in
  let t_after_wfreqs = Sys.time() in
  let t_solving = t_after_solving -. t_before_solving in
  let t_freqs = t_after_freqs -. t_before_freqs in
  let t_part = t_after_partition -. t_before_partition in
  let t_probs = t_after_probs -. t_before_probs in
  let t_wfreqs = t_after_wfreqs -. t_after_probs in
  let t_whole = t_after_wfreqs -. t_before_solving in
  if (!ignore_weights) then () else
    (    
      fprintf oc "weighted frequencies:\n";
      fprintf oc "%s\n" (freq_to_string wfreq);
      fprintf oc "\nTime to compute: %f\n" t_whole;
    );
  fprintf oc "solutions: %f\t frequencies: %f\t partitions: %f\t probs:%f\t weighted freqs:%f\n" t_solving t_freqs t_part  t_probs t_wfreqs;
  fprintf oc "=====================================================\n\n";
  flush oc;
  print_csv csvc top_file sensors  zones min_max max_max max_zone_size part t_whole t_solving t_freqs t_part t_probs t_wfreqs  ;
  Z3.Solver.get_statistics solver |> Z3.Statistics.to_string |> fprintf oc "%s\n\n"  ;
  flush oc;
  flush (snd log);
  (distributions, frequencies, wfreq)

let mk_analysis oc res =
  let is_unique row = match row with
    | (_,1,_) -> true
    | _ -> false
  in
  let unique_target_counts = List.filter (fun (dists, f,wf) -> List.length f = 1) res in
  let unique_distrs = List.filter (fun (dists, f,wf) -> (List.hd f)                                  
                                                        |> is_unique )
                        unique_target_counts
  in
  fprintf oc "unique number of targets: %d\n" (List.length unique_target_counts);
  fprintf oc "unique solutions: %d\n" (List.length unique_distrs);
  flush oc;
  ()
      
           
let usage_msg = "compute all possible exact sensor readings for given topology"
                 
let specs = [
    ("-f", Arg.String (fun s -> single := s), "analyse single file. will be overidden if directory to analyse is given");
    ("-d", Arg.String (fun s -> directory := s), "analyse all topologies in given directory");
    ("-o", Arg.String (fun s -> output := s), "prefix of output files. Will create two files: one with suffix .txt containing with verbose information, one .csv with comma separated values (Default \"result\")");
    ("-i", Arg.Set ignore_weights, "ignore weights and only analyse frequencies of target counts");
    ("-s", Arg.Int (fun i -> sruns := i), "number of simulation runs per partition block. If less or equal to zero, then full probability tree will be computed");
    ("-v", Arg.Set verbose, "verbose output");
    ("-u", Arg.Set uniform, "compute witnesses according to uniform distribution");
    ("-w", Arg.Int (fun i -> witnesses := i), "number of witnesses per model (only evaluated if -a is not set) (Default 100)");
  ]
           
let mk_full_path dir f =
  if Char.equal dir.[String.length dir -1] '/' then 
    dir^f
  else
    dir^"/"^f

let () =
  Random.self_init ();
  Arg.parse specs print_endline usage_msg;
  if (!single != "" && !directory = "") then
    let csvc = open_out (!output^".csv") in
    let oc = open_out (!output^".txt") in
    let _ = analyse_file oc csvc !single in
    close_out oc;
  else
  let files = Sys.readdir !directory
              |> Array.to_list 
              |> List.sort String.compare in
  let csvc = open_out (!output^".csv") in
  let oc = open_out (!output^".txt") in
  (*  let results = List.map (fun f ->  analyse_file oc (mk_full_path !directory f)) files*)
  let results = List.map (fun f -> mk_full_path !directory f
                                   |>  analyse_file oc csvc ) files
  in
  mk_analysis oc results;
  close_out oc
