open Util

type probtree = (float * (Zone.t * int ) * int Distr.t) tree

module Make (D : Util.Debug ) =
  struct


    let rec print_probtree  (tree:probtree) depth =
      let tabs = (copy_string "\t" depth) in
      match tree with
      | Leaf(w,z,d) ->  tabs ^ "(Zone: "^ (Zone.print_target_presence z) ^"\tProb: " ^ (string_of_float w)^"\tDistr:\n "^(Distr.print_indent d (depth+4)) ^tabs^")\n"
      | Node((w,z,d),l) ->
         let children = List.fold_right (fun t -> (^) (print_probtree t (depth+1))) l "" in 
         tabs ^ "(Zone: "^ (Zone.print_target_presence z) ^ "\tProb:" ^ (string_of_float w) ^ "\tDistr:\n"^(Distr.print_indent d (depth+4)) ^ "\n"^tabs^"Children: [\n"^ children ^ tabs^"])\n"



    let get_non_empty_ext_zones z d =
      List.filter (fun (zone,t) -> t > 0 && ((not (Zone.equal zone (fst z))) || (t <= snd z))) (Distr.bindings d)

    let remove_elt (z,r) l =
      let rec go l acc = match l with
        | [] -> List.rev acc
        | (z1,r1)::xs when  (Zone.equal z1 z && (r1 == r)) -> go xs acc
        | x::xs -> go xs (x::acc)
      in go l []

       
    let remove_duplicates l =
      let rec go l acc = match l with
        | [] -> List.rev acc
        | x :: xs -> go (remove_elt x xs) ( x ::acc)
      in go l []                        
       
    let compute_probtree weights distributions : probtree =
      let add_zone reading d = 
        match Distr.find_opt (fst reading) d with
        | None -> 
           Distr.add (fst reading) (1) d
        | Some(r) -> 
           Distr.add (fst reading) (r+1) d
      in
      let rec mk_children distrs p d z : probtree  =
        let pdistr = Distr.get_possible_supers d distrs in
        match pdistr with
        | [] -> failwith ("No possible extensions anymore!")
        | [el] -> Leaf(p,z,el)
        | _ ->
           let unique_zones = List.map (get_non_empty_ext_zones z)  pdistr
                              |> List.concat |> remove_duplicates
           in
           let extension_zones = unique_zones
                                 |> List.filter (fun (zone,reading) -> not ( Distr.exists (fun dzone dread -> ((Zone.equal dzone zone) && dread  = reading)) d  ) ) 
           in
           match extension_zones with
           | [] -> Leaf(p,  z, Distr.get_extension d pdistr )
           | hd :: tl ->
              let wsum = List.fold_right (fun (z,r) -> (+.) (W.find z weights)) extension_zones 0. in
              let subtrees = List.map (fun zone -> mk_children  pdistr ((W.find (fst zone) weights) /. wsum) (add_zone zone d) zone) extension_zones in
              Node((p,  z, d), subtrees)
      in
      mk_children  distributions  1. Distr.empty  (Zone.empty, 0)


    let get_prob_probtree (probs : probtree) (d : int Distr.t) prob = 
      let rec prob_aux (probs : probtree) dist p = match probs with
        | Leaf(pd, _, distr) -> if (Distr.equal (=) (distr) (dist)) then
                                  pd*.p
                                else
                                  0.
        | Node((pd, _, distr), children) -> if (Distr.is_subdistr distr dist) then 
                                              p *. (List.fold_right (fun c -> (+.) (prob_aux c dist (pd))) children 0.)
                                            else  0.
      in
      prob_aux probs d prob


    let mk_probabilities dlist weights p =
      match dlist with
      | [el] -> [(el, p)]
      | _ ->
         let tree = compute_probtree weights dlist in
         D.debug(print_probtree tree 1);
         List.map (fun d -> (d, get_prob_probtree tree d p)) dlist 
         
    let mk_probability_list partition zoneweights  =
      List.map (fun (c,p,ds) ->  (c,p, mk_probabilities ds zoneweights p)) partition 
      

    let simulate_target_count weights distributions =
      let add_zone reading d = 
        match Distr.find_opt (fst reading) d with
        | None -> 
           Distr.add ( fst reading) (1) d
        | Some(r) -> 
           Distr.add ( fst reading) (r+1) d
      in
      let rec aux distrs d z  =
        let pdistr = Distr.get_possible_supers d distrs in
        match pdistr with
        | [] -> failwith ("No possible extensions anymore!")
        | [el] -> el
        | _ ->
           let unique_zones = List.map (get_non_empty_ext_zones z)  pdistr
                              |> List.concat |> remove_duplicates
           in
           let extension_zones = unique_zones
                                 |> List.filter (fun (zone,reading) -> not ( Distr.exists (fun dzone dread -> ((Zone.equal dzone zone) && dread  = reading)) d  ) )
           in
           match extension_zones with
           | [] ->  Distr.get_extension d pdistr
           | [el] ->
              aux pdistr (add_zone el d) el
           | _ ->
              let wsum = List.fold_right (fun (z,r) -> (+.) (W.find z weights)) extension_zones 0. in
              let choose zones x =
                let rec caux zones x acc = match zones with
                  | [] -> failwith("No zone found!")
                  | hd :: tl ->
                     let weight = acc +. ( (W.find (fst hd) weights) /. wsum) in  
                     if (x <= weight) then hd else caux tl x weight
                in
                caux zones x 0.
              in
              let extension = choose extension_zones (Random.float 1.0) in
              aux pdistr (add_zone extension d) extension        
      in
      aux  distributions Distr.empty  (Zone.empty, 0)


    let mk_simulation log dlist weights p runs =
      let counts = List.map (Distr.count_targets) dlist in
      let max = find_max (fun x -> x) counts in 
      let t_before = Sys.time() in
      match dlist with
      | [el] -> [( el, p)]
      | _ ->
         let i = ref 0 in
         let freqs = Hashtbl.create(List.length dlist) in
         while (!i < runs) do
           i := !i+1;
           let simulated = simulate_target_count weights dlist in
           match Hashtbl.find_opt freqs simulated with
           | None -> Hashtbl.add freqs simulated (1)
           | Some(x) -> Hashtbl.replace freqs simulated (x+1)
         done;
         let counted_dists = hsh_to_list freqs in
         let result = List.map (fun (k,v) -> (k, float_of_int (v) /. float_of_int runs *. p)) counted_dists in
         let t_after = Sys.time() in
         print_string "Time for ";print_int runs;print_string " sims with blocksize ";print_int (List.length dlist);print_string " and max count ";print_int max;print_string": ";print_float (t_after -. t_before);print_endline "";
         log_csv (snd log) [string_of_int runs; string_of_int (List.length dlist); string_of_int max; string_of_float (t_after -. t_before)]; 
         result
         
    let mk_simulated_probability_list log partition zonew runs =
      List.map (fun (c,p,ds) ->  (c,p, mk_simulation log ds zonew p runs)) partition

      
  end
  (*
let rec print_probtree  (tree:probtree) depth =
  let tabs = (copy_string "\t" depth) in
  match tree with
  | Leaf(w,z,d) ->  tabs ^ "(Zone: "^ (Zone.print_target_presence z) ^"\tProb: " ^ (string_of_float w)^"\tDistr:\n "^(Distr.print_indent d (depth+4)) ^tabs^")\n"
  | Node((w,z,d),l) ->
     let children = List.fold_right (fun t -> (^) (print_probtree t (depth+1))) l "" in 
     tabs ^ "(Zone: "^ (Zone.print_target_presence z) ^ "\tProb:" ^ (string_of_float w) ^ "\tDistr:\n"^(Distr.print_indent d (depth+4)) ^ "\n"^tabs^"Children: [\n"^ children ^ tabs^"])\n"



let get_non_empty_ext_zones z d =
  List.filter (fun (zone,t) -> t > 0 && ((not (Zone.equal zone (fst z))) || (t <= snd z))) (Distr.bindings d)

let remove_elt (z,r) l =
  let rec go l acc = match l with
    | [] -> List.rev acc
    | (z1,r1)::xs when  (Zone.equal z1 z && (r1 == r)) -> go xs acc
    | x::xs -> go xs (x::acc)
  in go l []

   
let remove_duplicates l =
  let rec go l acc = match l with
    | [] -> List.rev acc
    | x :: xs -> go (remove_elt x xs) ( x ::acc)
  in go l []                        
   
let compute_probtree weights distributions : probtree =
  let add_zone reading d = 
    match Distr.find_opt (fst reading) d with
    | None -> 
       Distr.add (fst reading) (1) d
    | Some(r) -> 
       Distr.add (fst reading) (r+1) d
  in
  let rec mk_children distrs p d z : probtree  =
    let pdistr = Distr.get_possible_supers d distrs in
    match pdistr with
    | [] -> failwith ("No possible extensions anymore!")
    | [el] -> Leaf(p,z,el)
    | _ ->
       let unique_zones = List.map (get_non_empty_ext_zones z)  pdistr
                          |> List.concat |> remove_duplicates
       in
       let extension_zones = unique_zones
                             |> List.filter (fun (zone,reading) -> not ( Distr.exists (fun dzone dread -> ((Zone.equal dzone zone) && dread  = reading)) d  ) ) 
       in
       match extension_zones with
       | [] -> Leaf(p,  z, Distr.get_extension d pdistr )
       | hd :: tl ->
          let wsum = List.fold_right (fun (z,r) -> (+.) (W.find z weights)) extension_zones 0. in
          let subtrees = List.map (fun zone -> mk_children  pdistr ((W.find (fst zone) weights) /. wsum) (add_zone zone d) zone) extension_zones in
          Node((p,  z, d), subtrees)
  in
  mk_children  distributions  1. Distr.empty  (Zone.empty, 0)


let get_prob_probtree (probs : probtree) (d : int Distr.t) prob = 
  let rec prob_aux (probs : probtree) dist p = match probs with
    | Leaf(pd, _, distr) -> if (Distr.equal (=) (distr) (dist)) then
                              pd*.p
                            else
                              0.
    | Node((pd, _, distr), children) -> if (Distr.is_subdistr distr dist) then 
                                          p *. (List.fold_right (fun c -> (+.) (prob_aux c dist (pd))) children 0.)
                                        else  0.
  in
  prob_aux probs d prob


let mk_probabilities dlist weights p =
   match dlist with
  | [el] -> [(el, p)]
  | _ ->
  let tree = compute_probtree weights dlist in
  (*  print_endline (print_probtree tree 1);*)
     List.map (fun d -> (d, get_prob_probtree tree d p)) dlist 
     
let mk_probability_list partition zoneweights  =
  List.map (fun (c,p,ds) ->  (c,p, mk_probabilities ds zoneweights p)) partition 
  

let simulate_target_count weights distributions =
  let add_zone reading d = 
    match Distr.find_opt (fst reading) d with
    | None -> 
       Distr.add ( fst reading) (1) d
    | Some(r) -> 
       Distr.add ( fst reading) (r+1) d
  in
  let rec aux distrs d z  =
    let pdistr = Distr.get_possible_supers d distrs in
    match pdistr with
    | [] -> failwith ("No possible extensions anymore!")
    | [el] -> el
    | _ ->
       let unique_zones = List.map (get_non_empty_ext_zones z)  pdistr
                          |> List.concat |> remove_duplicates
       in
       let extension_zones = unique_zones
                             |> List.filter (fun (zone,reading) -> not ( Distr.exists (fun dzone dread -> ((Zone.equal dzone zone) && dread  = reading)) d  ) )
       in
       match extension_zones with
       | [] ->  Distr.get_extension d pdistr
       | [el] ->
          aux pdistr (add_zone el d) el
       | _ ->
          let wsum = List.fold_right (fun (z,r) -> (+.) (W.find z weights)) extension_zones 0. in
          let choose zones x =
            let rec caux zones x acc = match zones with
              | [] -> failwith("No zone found!")
              | hd :: tl ->
                 let weight = acc +. ( (W.find (fst hd) weights) /. wsum) in  
                 if (x <= weight) then hd else caux tl x weight
            in
            caux zones x 0.
          in
          let extension = choose extension_zones (Random.float 1.0) in
          aux pdistr (add_zone extension d) extension        
  in
  aux  distributions Distr.empty  (Zone.empty, 0)


let mk_simulation log dlist weights p runs =
  let counts = List.map (Distr.count_targets) dlist in
  let max = find_max (fun x -> x) counts in 
  let t_before = Sys.time() in
  match dlist with
  | [el] -> [( el, p)]
  | _ ->
  let i = ref 0 in
  let freqs = Hashtbl.create(List.length dlist) in
  while (!i < runs) do
    i := !i+1;
    let simulated = simulate_target_count weights dlist in
    match Hashtbl.find_opt freqs simulated with
    | None -> Hashtbl.add freqs simulated (1)
    | Some(x) -> Hashtbl.replace freqs simulated (x+1)
  done;
  let counted_dists = hsh_to_list freqs in
  let result = List.map (fun (k,v) -> (k, float_of_int (v) /. float_of_int runs *. p)) counted_dists in
  let t_after = Sys.time() in
  print_string "Time for ";print_int runs;print_string " sims with blocksize ";print_int (List.length dlist);print_string " and max count ";print_int max;print_string": ";print_float (t_after -. t_before);print_endline "";
  log_csv (snd log) [string_of_int runs; string_of_int (List.length dlist); string_of_int max; string_of_float (t_after -. t_before)]; 
  result
    
let mk_simulated_probability_list log partition zonew runs =
  List.map (fun (c,p,ds) ->  (c,p, mk_simulation log ds zonew p runs)) partition

      *)
