open Util
open Z3.Arithmetic
open Z3.Boolean
open Z3.Expr
open Z3.Symbol

module Make (D : Debug ) =
  struct
    
    let rec read_lines out file =
      try read_lines ((input_line file) :: out) file with
      | End_of_file -> (close_in file; List.rev out)

    let parse file =
      open_in file
      |> read_lines []

    let tokenize = String.split_on_char ' '

    let tokenize_zone line =
      let tokens = (String.split_on_char ' ' line) in
      match tokens  with
      |[] | [_]| [_;_] -> failwith ("Error: Invalid input file! Reason: invalid zone description " ^ line)
      |weight :: target :: desc -> (float_of_string weight, int_of_string target, desc) 

    let rec mk_rprob r ran probs = match (ran,probs) with
      | ([],[]) -> r
      | (hd :: tl, phd :: ptl) -> mk_rprob (ReadingProb.add hd phd r) tl ptl
      | _ -> failwith ("error distribution does not match range for sensor")                                      

    let rec parse_rdist sprob sens mins maxs lines  = match lines with
      | [] -> failwith ("No topological information in file")
      | hd :: tl -> match sens with
                    | [] -> (sprob, List.map tokenize_zone (hd::tl), List.length tl)
                    | shd :: stl ->
                       let probs = List.map float_of_string (tokenize hd) in
                       if (List.length probs !=
                             ((snd (List.find (fun (s,r) -> String.equal s shd ) maxs)) - 
                                (snd (List.find (fun (s,r) -> String.equal s shd ) mins))+1)
                          )
                       then failwith ("length of error probabilities does not match possible values for sensor reading")
                       else
                         let ran = range (snd (List.find (fun (s,r) -> String.equal s shd ) mins)) (snd (List.find (fun (s,r) -> String.equal s shd ) maxs))
                         in
                         parse_rdist (SensorsProb.add shd (mk_rprob ReadingProb.empty ran probs) sprob) stl mins maxs tl  
                         
    let parse_topology fname =
      match parse fname with
      | [] | [_] | [_;_]-> failwith ("Error: Invalid input file " ^ fname)
      | x :: y :: z :: xs ->
         let ids = tokenize x       
         in
         let min = tokenize y
                   |> List.map2 (fun s r -> (s, int_of_string r)) ids 
         in
         let max = tokenize z
                   |> List.map2 (fun s r -> (s, int_of_string r)) ids 
         in
         let (sprobs, rel, len) = parse_rdist SensorsProb.empty ids min max xs 
         in
         (ids, min, max, rel, len, sprobs)


    let mk_zones raw_zones =
      let zones = List.map Zone.rem_meta raw_zones in
      let rec zones_aux raw_zones weights = match raw_zones with
        | [] -> weights
        | (w,t,desc) :: tl -> zones_aux tl (W.add (Zone.of_list desc) w weights)
      in
      (zones_aux raw_zones W.empty, [], zones)

    let mk_zone_consts ctx =
      List.map (fun z -> (z, mk_string ctx (String.concat "" (Zone.elements z)))) 

    let rec mk_sensor_sum ctx domain zones sensor =
      match zones with
      | [] -> (mk_numeral_int ctx 0 domain)
      | hd :: tl ->
         let (name, symb) = hd in
         if (Zone.exists ((String.equal) sensor) name)
         then mk_add ctx [(Z3.Expr.mk_const ctx symb domain) ; (mk_sensor_sum ctx domain tl sensor)]
         else  mk_sensor_sum ctx domain tl sensor
         
    let mk_sensor_equation ctx domain zones reading =
      let (sensor, value) = reading in
      mk_eq ctx (mk_numeral_int ctx value domain) (mk_sensor_sum ctx domain zones sensor) 

    let mk_sensor_minimum ctx domain zones reading =
      let (sensor, value) = reading in
      Z3.Arithmetic.mk_le ctx (mk_numeral_int ctx value domain) (mk_sensor_sum ctx domain zones sensor) 

    let mk_sensor_maximum ctx domain zones reading =
      let (sensor, value) = reading in
      Z3.Arithmetic.mk_ge ctx (mk_numeral_int ctx value domain) (mk_sensor_sum ctx domain zones sensor) 
      
      
    let mk_topologic_constraints ctx domain min max max_value zones  = 
      let pos =
        List.map (fun (_,z) -> Z3.Arithmetic.mk_ge ctx (Z3.Expr.mk_const ctx z domain) (mk_numeral_int ctx 0 domain)
          ) zones
      in
      let max_values =
        List.map (fun (_,z) -> Z3.Arithmetic.mk_le ctx (Z3.Expr.mk_const ctx z domain) (mk_numeral_int ctx (max_value) domain)
          ) zones
      in
      
      let mins =
        List.map (fun r ->  mk_sensor_minimum ctx domain zones r ) min
      in
      let maxs =
        List.map (fun r -> mk_sensor_maximum ctx domain zones r) max
      in
      List.append (List.append pos max_values) (List.append mins maxs)
  end
