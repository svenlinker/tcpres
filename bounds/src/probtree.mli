module Make :
functor (D : Util.Debug) ->
sig

  val mk_probabilities :
    int Util.Distr.t list ->
    float Util.W.t -> float -> (int Util.Distr.t * float) list
  val mk_probability_list :
    ('a * float * int Util.Distr.t list) list ->
    float Util.W.t -> ('a * float * (int Util.Distr.t * float) list) list
  val simulate_target_count :
    float Util.W.t -> int Util.Distr.t list -> int Util.Distr.t
  val mk_simulation : 'a * out_channel -> int Util.Distr.t list -> float Util.W.t -> float -> int -> (int Util.Distr.t * float) list

  val mk_simulated_probability_list : 'a * out_channel -> ('b * float * int Util.Distr.t list) list -> float Util.W.t -> int -> ('b * float * (int Util.Distr.t * float) list) list
end
