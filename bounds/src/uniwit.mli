module Make :
  functor (D : Util.Debug) ->
    sig
      val compute_pivot : int -> float -> int
      val mk_hash_int :
        Z3.context -> Z3.Sort.sort -> Z3.Expr.expr -> int -> Z3.Expr.expr
      val mk_vector_hash :
        Z3.context ->
        Z3.Sort.sort -> Z3.FuncDecl.func_decl list -> int -> Z3.Expr.expr
      val compute_hash_value :
        Z3.Model.model option -> int -> int -> int -> int -> int 
      val mk_mod_axioms :
        Z3.context ->
        Z3.Sort.sort ->
        int ->
        Z3.FuncDecl.func_decl list ->
        Z3.FuncDecl.func_decl * Z3.FuncDecl.func_decl * Z3.Expr.expr list
      val mk_hash_formula :
        Z3.context ->
        Z3.Sort.sort ->
        Z3.FuncDecl.func_decl list ->
        int -> int -> Z3.Expr.expr * Z3.Expr.expr
      val log2 : float -> float
      val get_next_prime : int -> int
(*      val model_to_constraints :
        Z3.context -> Z3.Sort.sort -> Z3.Model.model option -> Z3.Expr.expr
 *)
      val get_models_iter_expr :
        Z3.context ->
        Z3.Sort.sort ->
        Z3.Solver.solver ->
        Z3.Expr.expr list -> int -> Z3.Model.model option list
      val get_models_iter :
        Z3.context ->
        Z3.Sort.sort -> Z3.Solver.solver -> int -> Z3.Model.model option list
      val uniwit :
        Z3.context ->
        Z3.Sort.sort ->
        Z3.Solver.solver ->
        Z3.Expr.expr list -> int -> float -> Z3.Model.model option
    end
