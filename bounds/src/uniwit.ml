open List
open Z3
open Z3.Solver
open Z3.Model
open Z3.Boolean
open Z3.Arithmetic
open Z3.Arithmetic.Integer
open Util
   
   
module Make (D : Util.Debug ) =
  struct
    
    let compute_pivot n k =
      let pivot = int_of_float (ceil (2. *. ((float_of_int n) ** (1. /. k)))) in
      (*  let pivot = 2 * n*n in *)
      pivot

      
    (* Universal Hash function x -> ((a*x + b) mod p) mod m *)  
    let mk_hash_int ctx domain value  m =
      let a = Random.int (m-1) +1 in
      (*    let b = Random.int (p) + 1 in*)
      (*  let h = mk_mod ctx (mk_add ctx [mk_mul ctx [Z3.Expr.mk_numeral_int ctx a domain; value] ; Z3.Expr.mk_numeral_int ctx b domain]) (Z3.Expr.mk_numeral_int ctx p domain) in
  mk_mod ctx h (Z3.Expr.mk_numeral_int ctx m domain)  *)
      mk_mul ctx [Z3.Expr.mk_numeral_int ctx a domain;value]

    (* Universal Hash function for vectors/lists: sum of hash values modulo m  *)  
    let mk_vector_hash ctx domain funcs m =
      (*  let _ = print_endline "constructing hash vector" in*)
      mk_mod  ctx (mk_add ctx (List.map (fun f -> mk_hash_int ctx domain (Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name f) domain) m) funcs)) (Z3.Expr.mk_numeral_int ctx m domain) 
      
      
    let compute_hash_value model a b p m =
      match model with
      | None -> 0
      | Some model ->
         let funcs = get_const_decls model in
         let interpret = fun f ->
           match (get_const_interp model f) with
           | None -> 0 
           | Some exp -> Integer.get_big_int exp |> Big_int.int_of_big_int 
         in
         let result =   (List.fold_right (+) (List.map (fun f ->
                                                  ((a * interpret f) + b) mod p ) funcs ) 0) mod m
         in
         print_string "hash:";print_int result;print_endline "";
         result
 
    let mk_mod_axioms ctx domain k vars =
      let mod_def = Z3.FuncDecl.mk_func_decl ctx (Z3.Symbol.mk_string ctx ("mod"^(string_of_int k))) [domain] domain in
      let quot_def = Z3.FuncDecl.mk_func_decl ctx (Z3.Symbol.mk_string ctx ("quot"^(string_of_int k))) [domain] domain in
      (*  let ax_ge_zero = List.map (fun v -> mk_le ctx (Z3.Arithmetic.Integer.mk_numeral_i ctx 0) (Z3.FuncDecl.apply mod_def [Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name v) domain]  )) vars in
  let ax_le_k= List.map (fun v -> mk_lt ctx  (Z3.FuncDecl.apply mod_def [Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name v) domain] ) (Z3.Arithmetic.Integer.mk_numeral_i ctx k)) vars in
  let ax_def = List.map (fun v -> mk_eq ctx  (
                                      mk_add ctx
                                        [Z3.FuncDecl.apply mod_def
                                           [Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name v) domain] ;
                                         mk_mul ctx
                                           [Z3.Arithmetic.Integer.mk_numeral_i ctx k;
                                            Z3.FuncDecl.apply quot_def
                                              [Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name v) domain]
                                           ]
                                        ]
                                    )
                                    (Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name v) domain)
                 )
                 vars in
  let quot_ge_zero = List.map (fun v -> mk_le ctx (Z3.Arithmetic.Integer.mk_numeral_i ctx 0) (Z3.FuncDecl.apply quot_def [Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name v) domain]  )) vars in

       *)
      let var_name = Z3.Symbol.mk_string ctx "x_0" in
      let var = Z3.Quantifier.mk_bound ctx 0 domain in
      let ax_ge_zero_body = mk_le ctx (Z3.Arithmetic.Integer.mk_numeral_i ctx 0) (Z3.FuncDecl.apply mod_def [var]  )  in
      let ax_ge_zero = Z3.Quantifier.expr_of_quantifier (Z3.Quantifier.mk_forall ctx [domain] [var_name] ax_ge_zero_body (Some 1) [] [] None None) in
      let ax_le_k_body=  mk_lt ctx  (Z3.FuncDecl.apply mod_def [var]) (Z3.Arithmetic.Integer.mk_numeral_i ctx k) in 
      let ax_le_k= Z3.Quantifier.expr_of_quantifier (Z3.Quantifier.mk_forall ctx [domain] [var_name] ax_le_k_body (Some 1) [] [] None None) in
      let ax_def_body  = mk_eq ctx  (
                             mk_add ctx
                               [Z3.FuncDecl.apply mod_def
                                  [var] ;
                                mk_mul ctx
                                  [Z3.Arithmetic.Integer.mk_numeral_i ctx k;
                                   Z3.FuncDecl.apply quot_def
                                     [var]
                                  ]
                               ]
                           )
                           (var) 
                       
      in
      let ax_def= Z3.Quantifier.expr_of_quantifier (Z3.Quantifier.mk_forall ctx [domain] [var_name] ax_def_body (Some 1) [] [] None None) in

      let quot_ge_zero = List.map (fun v -> mk_le ctx (Z3.Arithmetic.Integer.mk_numeral_i ctx 0) (Z3.FuncDecl.apply quot_def [Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name v) domain]  )) vars in
      print_string "constructed: ";(List.iter (fun result  -> print_endline (Z3.Expr.to_string result)) (concat [[ax_ge_zero; ax_le_k; ax_def];quot_ge_zero]));
      (mod_def, quot_def, (concat [[ax_ge_zero;ax_le_k;ax_def];quot_ge_zero]))
      
    (* Construct h(model) =  \alpha as Z3 formula *) 
    let mk_hash_formula ctx domain funcs m alpha  =
      (*  match model with
  | None -> mk_true ctx
  | Some model ->*)
      (*  print_endline "build hash formula"; *)
      let vec_hash = (mk_vector_hash ctx domain funcs  m) in 
      (*     let funcs = get_const_decls model in*)
      let result =      mk_eq ctx (vec_hash)  (Z3.Expr.mk_numeral_int ctx (alpha) domain) in
      (result, vec_hash)

      
    (* Universal Hash function x -> ((a*x + b) mod p) mod m *)  
    (*let mk_hash_int_manual ctx domain value prime mod_p_def mod_m_def =
    let a = Random.int (prime) + 1 in
    let b = Random.int (prime) + 1 in
    let h = Z3.FuncDecl.apply mod_p_def
              [mk_add ctx [mk_mul ctx [Z3.Expr.mk_numeral_int ctx a domain; value] ; Z3.Expr.mk_numeral_int ctx b domain]] in
 Z3.FuncDecl.apply mod_m_def [h] 

(* Universal Hash function for vectors/lists: sum of hash values modulo m  *)  
let mk_vector_hash_manual ctx domain funcs  prime mod_p_def mod_m_def =
  (*  let (mod_p_def, quot_p_def, axioms_p) = p in*)
  (*  let _ = print_endline "constructing hash vector" in*)
  Z3.FuncDecl.apply mod_m_def [mk_add ctx (List.map (fun f -> mk_hash_int_manual ctx domain (Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name f) domain) prime mod_p_def mod_m_def) funcs)]
(*mk_mod  ctx (mk_add ctx (List.map (fun f -> mk_hash_int ctx domain (Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name f) domain) p m) funcs)) (Z3.Expr.mk_numeral_int ctx m domain) *)
  
let mk_hash_formula_manual ctx domain consts p m alpha =
  let (mod_p_def, quot_p_def, axioms_p) = mk_mod_axioms ctx domain p consts in
  let (mod_m_def, quot_m_def, axioms_m) = mk_mod_axioms ctx domain m consts in
  let vec_hash = mk_vector_hash_manual ctx domain consts p mod_p_def mod_m_def   in
  let result =      mk_eq ctx (vec_hash)  (Z3.Expr.mk_numeral_int ctx (alpha) domain) in
    print_string "constructed: ";print_endline (Z3.Expr.to_string result);
  (result,vec_hash, concat [ axioms_p;axioms_m])
     *)  

    let log2 x =
      (log10 x) /. (log10 2.)

    let get_next_prime n =
      let is_prime x = if ( x>= 0) && (x <= 2) then false
                       else
                         let rec is_not_divisor d =
                           d * d > x || (x mod d <> 0 && is_not_divisor (d+1))
                         in
                         is_not_divisor 3
      in
      let rec aux x = if (is_prime x) then x else aux (x+2)
      in
      let result = if ((n mod 2) = 0) then aux (n+1) else aux (n+2)
      in
      result

(*    let model_to_constraints ctx domain model =
      match model with
      | None -> mk_true ctx
      | Some m -> 
         let funcs = get_const_decls m in
         let interpret = fun f ->
           match (get_const_interp m f) with
           | None -> Z3.Expr.mk_numeral_int ctx 0 domain 
           | Some exp -> exp
         in
         let negated = fun f -> mk_not ctx (mk_eq ctx (Z3.Expr.mk_const ctx (Z3.FuncDecl.get_name f)  domain)  (interpret f))
         in     
         (mk_or ctx (List.map negated funcs)) 
 *)        
         
    (* re-initialises solver with given eqs and computes up to r solutions*)  
    let get_models_iter_expr ctx domain solver eqs r =
      (* save solver state *)  
      Z3.Solver.push solver;
      (* add restrictions to model *)
      Z3.Solver.add solver eqs;
      let solutions = ref [] in
      let i = ref 0 in
      D.debug("running bounded SAT");
      while (check solver [] = SATISFIABLE) && !i < r  do
        let model = get_model solver in
        let negated = SolveUtil.model_to_constraints ctx domain model  in
        solutions := model :: !solutions;
        i := !i +1 ;
        Z3.Solver.add solver [negated];
      done;
      (* reset solver state to initial *)
      Z3.Solver.pop solver 1;
      !solutions

    let get_models_iter ctx domain solver r =
      (* save current state of solver *)
      Z3.Solver.push solver;
      let solutions = ref [] in
      let i = ref 0 in
      while (check solver [] = SATISFIABLE) && !i < r  do
        let model = get_model solver in
        let negated = SolveUtil.model_to_constraints ctx domain model  in
        solutions := model :: !solutions;
        i := !i +1 ;
        Z3.Solver.add solver [negated];
      done;
      (* reset solver state to initial *)
      Z3.Solver.pop solver 1;
      !solutions
      
      
      
      
    let uniwit ctx domain solver eqs n k =
      (* run Z3 initially to analyse the model *)
      Z3.Solver.add solver eqs;
      let _ = Z3.Solver.check solver [] in
      let pivot = compute_pivot n k in
      D.debug ("pivot:"^(string_of_int pivot));
      let solutions = ref (get_models_iter ctx domain solver  (pivot+1)) in 
      if (List.length !solutions <= pivot) then
        let j = Random.int (List.length !solutions) in
        List.nth !solutions j;   
      else
        (
          let funcs = match (List.hd !solutions ) with
              None -> failwith ("No models to iterate over!!")
            | Some m -> Z3.Model.get_const_decls m
          in
          let p = get_next_prime (List.length !solutions) in
          D.debug ("p:"^(string_of_int p));
          let l = int_of_float (floor ((1. /. k) *. (log2 (float_of_int n)))) in
          D.debug("l:"^(string_of_int l));
          let i = ref (l +1) in
          while ((List.length !solutions > pivot || List.length !solutions == 0) && (!i < n))  do
            D.debug ("currently having "^(string_of_int (List.length !solutions))^" solutions");
            let m = int_of_float (2. ** float_of_int (!i - l)) in
            D.debug("m:"^(string_of_int  m));
            let alpha = Random.int m in
            D.debug ("alpha:"^(string_of_int alpha));
            let (hash, expr) = mk_hash_formula ctx domain funcs m alpha in
            (*        solutions := get_models_iter_expr ctx domain solver expr [hash] (pivot+1);*) 
            solutions := get_models_iter_expr ctx domain solver [hash] (pivot+1); 
            i := !i +1
          done;
          if (List.length !solutions > pivot || List.length !solutions < 1) then
            None
          else
            let j = Random.int (pivot) in
            D.debug ("j:"^(string_of_int j)^"\t solutions:"^(string_of_int (List.length !solutions)));
            if (j < List.length !solutions) then 
              List.nth !solutions j
            else
              None
        )


  end
