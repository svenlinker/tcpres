file: ./weights.top
number of zones:8
maximal min:2 	maximal max:4	maximal diff:3
maximal zone size: 3
creating all distributions
number of solutions: 539
unweighted frequencies:
2:	 7	0.012987012987
3:	 45	0.0834879406308
4:	 116	0.215213358071
5:	 158	0.293135435993
6:	 129	0.239332096475
7:	 63	0.116883116883
8:	 18	0.0333951762523
9:	 3	0.00556586270872

different sensor readings: 66
largest block: 29

weighted frequencies:
2:	 7	0.00059567190713
3:	 45	0.00866235591655
4:	 116	0.0458385277123
5:	 158	0.150838353486
6:	 129	0.315106646102
7:	 63	0.320763219302
8:	 18	0.139326768518
9:	 3	0.0188684570557


Time to compute: 27.484224
solutions: 0.087966	 frequencies: 0.000232	 partitions: 0.002327	 probs:27.359270	 weighted freqs:0.010832
=====================================================

(:arith-bound-propagations-cheap 18300
 :arith-bound-propagations-lp    185
 :arith-conflicts                18
 :arith-lower                    13322
 :arith-make-feasible            4074
 :arith-max-columns              15
 :arith-max-rows                 3
 :arith-propagations             18300
 :arith-rows                     18354
 :arith-upper                    5024
 :binary-propagations            8998
 :conflicts                      327
 :decisions                      3945
 :del-clause                     1002
 :final-checks                   538
 :max-memory                     4.30
 :memory                         4.30
 :minimized-lits                 869
 :mk-bool-var                    70
 :mk-clause                      1002
 :num-allocs                     3968684
 :num-checks                     539
 :propagations                   16876
 :rlimit-count                   391721)

