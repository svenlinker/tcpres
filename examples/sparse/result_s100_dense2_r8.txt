file: s100_dense2_r8.top
number of sensors:100
number of zones:140
maximal min:0 	maximal max:2	maximal diff:2
maximal zone size: 8
creating all distributions
number of solutions: 24833
unweighted frequencies:
0:	 1	4.02689968993e-05
1:	 48	0.00193291185117
2:	 574	0.0231144042202
3:	 2858	0.115088793138
4:	 6730	0.271010349132
5:	 7865	0.316715660613
6:	 4826	0.194338179036
7:	 1611	0.0648733540048
8:	 292	0.0117585470946
9:	 27	0.00108726291628
10:	 1	4.02689968993e-05

different sensor readings: 576
largest block: 1665

solutions: 35.676758	 frequencies: 0.181285	 partitions: 39.192460	 probs:0.000001	 weighted freqs:0.000001
=====================================================

(:arith-bound-propagations-cheap 2340371
 :arith-bound-propagations-lp    767190
 :arith-conflicts                717
 :arith-lower                    2120651
 :arith-make-feasible            830619
 :arith-max-columns              152
 :arith-max-rows                 8
 :arith-propagations             2340371
 :arith-rows                     2363621
 :arith-upper                    242962
 :binary-propagations            450847
 :conflicts                      15130
 :decisions                      764422
 :del-clause                     468
 :final-checks                   24832
 :max-memory                     338.25
 :memory                         338.25
 :minimized-lits                 107353
 :mk-bool-var                    536
 :mk-clause                      40302
 :num-allocs                     82277097065.00
 :num-checks                     24833
 :propagations                   959401
 :rlimit-count                   380591551)

